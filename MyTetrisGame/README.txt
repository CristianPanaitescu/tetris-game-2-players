Commands: 

	Enter - Start the game
	BackSpace - Reset the game (press enter to start again)
	Esc - Exit the game
	
Left player commands:

	W - Up
	S - Down
	A - Rotate
	D - Advance
	TAB - Drop

Right player commands:

	Arrow_Up - Up
	Arrow_Down - Down
	Arrow_Right - Rotate
	Arrow_Left - Advance
	LCTRL - Drop

References:
	http://javilop.com/gamedev/tetris-tutorial-in-c-platform-independent-focused-in-game-logic-for-beginners/
	This channel: https://www.youtube.com/user/YouKondziu/videos