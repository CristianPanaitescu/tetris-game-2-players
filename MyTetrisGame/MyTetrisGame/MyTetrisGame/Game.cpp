#include "Game.h"

using namespace std;

/*An event queue where i add all the events that occur in a game.*/
queue<Event*> Game::eventQueue;

/*Boolean variable which determing if the game has started or not*/
bool Game::hasStarted;

/*Has the initial time between two moments when the piece advance*/
long Game::initialTime;

/*Public constructor that builds the board, the pieces and an interface. Also inits a game*/
Game::Game(Board *pBoard, Pieces *pPieces, Interface *pInterface)
{
	Game::hasStarted = false;
	mBoard = pBoard;
	mPieces = pPieces;
	mInterface = pInterface;

	InitGame();

}

/*Method that draw the whole scene for the game: the background image, the board,
the middle line and both players moving pieces*/
void Game::drawScene(SDL_Renderer *ren) {
	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren); // clear the window


	SDL_Rect bg;
	bg.x = 0;
	bg.y = 0;
	bg.h = 500;
	bg.w = 1000;

	SDL_RenderCopy(ren, background, NULL, &bg); // draw the background

	drawBoard(ren); // draw the board

	if (!hasStarted) {
		SDL_RenderPresent(ren);
		return;
	}

	mInterface->DrawRectangle(ren,
		BOARD_POSITION_X + mBoard->middle * BLOCK_SIZE - 2,
		BOARD_POSITION_Y - 1,
		BOARD_HEIGHT * BLOCK_SIZE + 1,
		MIDDLE_LINE_WIDTH,
		CYAN);  // draw the middle line


	drawAllPieces(ren); // draw the 4 pieces (2 moving and 2 out of the board)


	SDL_RenderPresent(ren); // render everything

}

/*Method that inits the players*/
void Game::InitGame()
{
	leftPlayer = new Player(1);
	rightPlayer = new Player(2);

	leftPlayer->init(mPieces);
	rightPlayer->init(mPieces);
}

/*Method that does the move of the pieces every "DROP_TIME" ms, currently set to 700ms*/
void Game::doTheMove() {

	// if left players piece can move, do the move
	if (mBoard->IsPossibleMovement(1, leftPlayer->mPosX + 1, leftPlayer->mPosY, leftPlayer->mPiece, leftPlayer->mRotation))
		leftPlayer->mPosX++;
	else { // else store it in the board and create new piece
		mBoard->StorePiece(leftPlayer->mPosX, leftPlayer->mPosY, leftPlayer->mPiece, leftPlayer->mRotation);
		leftPlayer->CreateNewPiece();
	}
	// if right players piece can move, do the move
	if (mBoard->IsPossibleMovement(2, rightPlayer->mPosX - 1, rightPlayer->mPosY, rightPlayer->mPiece, rightPlayer->mRotation))
		rightPlayer->mPosX--;
	else{// else store it in the board and create new piece
		mBoard->StorePiece(rightPlayer->mPosX, rightPlayer->mPosY, rightPlayer->mPiece, rightPlayer->mRotation);
		rightPlayer->CreateNewPiece();
	}

}

/*Method that solves a move event*/
void Game::solveMoveEvent(int side, int advance, int direction){

	if (side == 1) { // if left player
		if (mBoard->IsPossibleMovement(side, leftPlayer->mPosX + advance, leftPlayer->mPosY - direction,
			leftPlayer->mPiece, leftPlayer->mRotation)) { // if piece can move
			leftPlayer->mPosX += advance; // move piece horisontaly
			leftPlayer->mPosY -= direction; // move piece verticaly
		}
	}
	else // else right player
		if (mBoard->IsPossibleMovement(side, rightPlayer->mPosX + advance, rightPlayer->mPosY - direction,
			rightPlayer->mPiece, rightPlayer->mRotation)) { // if piece can move
		rightPlayer->mPosX += advance; // move piece horisontaly
		rightPlayer->mPosY -= direction; // move piece verticaly
		}
}

/*Solve rotate event*/
void Game::solveRotateEvent(int side) {
	if (side == 1) {
		if (mBoard->IsPossibleMovement(side, leftPlayer->mPosX, leftPlayer->mPosY,
			leftPlayer->mPiece, (leftPlayer->mRotation + 1) % 4))
			leftPlayer->mRotation = (leftPlayer->mRotation + 1) % 4;
	}
	else
		if (mBoard->IsPossibleMovement(side, rightPlayer->mPosX, rightPlayer->mPosY,
			rightPlayer->mPiece, (rightPlayer->mRotation + 1) % 4))
			rightPlayer->mRotation = (rightPlayer->mRotation + 1) % 4;
}

/*Solve drop event*/
void Game::solveDropEvent(int side) {
	if (side == 1) { // left player
		// while can move
		while (mBoard->IsPossibleMovement(1, leftPlayer->mPosX + 1, leftPlayer->mPosY, leftPlayer->mPiece, leftPlayer->mRotation))
			leftPlayer->mPosX++; // move the piece until hits a block

		//store it in the board and create a new one
		mBoard->StorePiece(leftPlayer->mPosX, leftPlayer->mPosY, leftPlayer->mPiece, leftPlayer->mRotation);
		leftPlayer->CreateNewPiece();

	}
	else { //same as left player
		while (mBoard->IsPossibleMovement(2, rightPlayer->mPosX - 1, rightPlayer->mPosY, rightPlayer->mPiece, rightPlayer->mRotation))
			rightPlayer->mPosX--;

		mBoard->StorePiece(rightPlayer->mPosX, rightPlayer->mPosY, rightPlayer->mPiece, rightPlayer->mRotation);
		rightPlayer->CreateNewPiece();
	}

}

/*Solve a reset event and in case of finish print a winner*/
void Game::solveResetEvent(int winner) {
	hasStarted = false;
	InitGame();
	mBoard = new Board(mPieces);

	switch (winner) {
		case 1: mBoard->printP1(); break;
		case 2: mBoard->printP2(); break;
		case 0: mBoard->printDraw(); break;
	}
}

/*Methode that solves an event from the events queue*/
void Game::solveEvent(Event* e) {
	if (!hasStarted && e->type != 0 && e->type != 5)
		return;
	switch (e->type) {
	case 0: // Start
		if (!hasStarted) {
			initialTime = SDL_GetTicks();
			hasStarted = true;
			mBoard = new Board(mPieces);
		}
		break;

	case 1: // Reset
		solveResetEvent(((ResetEvent*)e)->winner);
		break;

	case 2: // Move
		solveMoveEvent(((MoveEvent*)e)->side, ((MoveEvent*)e)->advance, ((MoveEvent*)e)->direction);
		break;

	case 3: // Rotate
		solveRotateEvent(((RotateEvent*)e)->side);
		break;
	case 4: // Drop
		solveDropEvent(((DropEvent*)e)->side);
		break;
	case 5: // Exit
		exit(5);
	}
}


/*Method that has the main loop, generating the frames.*/
void Game::run() {

	//init the SDL
	SDL_Init(SDL_INIT_EVERYTHING);

	//define a window with fixed dimensions
	SDL_Window *win = SDL_CreateWindow("MyTetrisGame",
		WINDOW_POSITION_X, WINDOW_POSITION_Y, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_SHOWN);

	//define a renderer which will draw the actual game
	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED);

	// Load the background image into the renderer
	background = IMG_LoadTexture(ren, "bg.bmp");

	// Event used for QUIT
	SDL_Event e;
	SDL_PollEvent(&e);

	//the loop
	while (e.type != SDL_QUIT) {

		SDL_PollEvent(&e);

		// solve the events from the queue
		while (!eventQueue.empty()) {

			solveEvent(eventQueue.front());
			eventQueue.pop();
		}

		// draw the game using the renderer
		drawScene(ren);

		// check if pieces should drop
		if ((SDL_GetTicks() - initialTime > DROP_TIME) && hasStarted) {
			initialTime = SDL_GetTicks(); // update initial time
			mBoard->DeletePossibleLines(); // delete the fully filled lines
			doTheMove(); // move both pieces un position towards the middle line
			mBoard->IsGameOver(); // check is game is over

		}
	}

	// destroy the window and the renderer and quit SDL
	SDL_DestroyWindow(win);
	SDL_DestroyRenderer(ren);
	SDL_Quit();
}

// Draw a piece at pX, pY coors using the renderer
void Game::drawPiece(SDL_Renderer* ren, int pX, int pY, int pPiece, int pRotation)
{
	// Color of the block 
	color mColor;
	int mX, mY;

	if (pY != OUTER_POSITION) {
		mX = BOARD_POSITION_X + pX * BLOCK_SIZE;
		mY = BOARD_POSITION_Y + pY * BLOCK_SIZE;
	}
	else { //next Block
		mX = BOARD_POSITION_X + pX * BLOCK_SIZE;
		mY = 20;
	}


	// For each block in the piece 
	for (int i = 0; i < PIECE_BLOCKS; i++)
	{
		for (int j = 0; j < PIECE_BLOCKS; j++)
		{
			// Get the type of the piece and draw it with the correct color
			switch (pPiece)
			{
				case 0: mColor = YELLOW; break;		// Color for SQUARE
				case 1: mColor = BLUE; break;		// Color for LINE
				case 2: mColor = ORANGE; break;		// Color for L
				case 3: mColor = PINK; break;		// Color for mirrored-L
				case 4: mColor = RED; break;		// Color for N
				case 5: mColor = GREEN; break;		// Color for mirrored-N
				case 6: mColor = MAGENTA; break;	// Color for T
			}

			if (mPieces->GetBlockType(pPiece, pRotation, j, i) != 0)
				mInterface->DrawRectangle(ren,
				mX + i * BLOCK_SIZE,
				mY + j * BLOCK_SIZE,
				BLOCK_SIZE - 1,
				BLOCK_SIZE - 1,
				mColor);
		}
	}
}

/*Draw the pieces in the board*/
void Game::drawAllPieces(SDL_Renderer* ren) {
	drawPiece(ren, leftPlayer->mPosX, leftPlayer->mPosY, leftPlayer->mPiece, leftPlayer->mRotation); // player1 piece
	drawPiece(ren, leftPlayer->mNextPosX, leftPlayer->mNextPosY, leftPlayer->mNextPiece, leftPlayer->mNextRotation); // player1 next piece
	drawPiece(ren, rightPlayer->mPosX, rightPlayer->mPosY, rightPlayer->mPiece, rightPlayer->mRotation); // player2 piece
	drawPiece(ren, rightPlayer->mNextPosX, rightPlayer->mNextPosY, rightPlayer->mNextPiece, rightPlayer->mNextRotation); // player2 next piece

}

/*Draw the board*/
void Game::drawBoard(SDL_Renderer* ren)
{

	//For each block in the board
	for (int i = 0; i < BOARD_WIDTH; i++)
	{
		for (int j = 0; j < BOARD_HEIGHT; j++)
		{
			// Check if the block is filled, if so, draw it
			if (!mBoard->IsFreeBlock(i, j))
				mInterface->DrawRectangle(ren,
				BOARD_POSITION_X + i * BLOCK_SIZE,
				BOARD_POSITION_Y + j * BLOCK_SIZE,
				BLOCK_SIZE - 1,
				BLOCK_SIZE - 1,
				DARK_CYAN);
		}
	}

}

