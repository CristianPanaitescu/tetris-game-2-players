#include "Interface.h"

Interface::Interface()
{
}

void Interface::DrawRectangle(SDL_Renderer* ren, int x, int y, int h, int w, color c) {

	switch (c) {
	case DARK_CYAN:
		SDL_SetRenderDrawColor(ren, 0, 130, 130, 255);
		break;
	case CYAN:
		SDL_SetRenderDrawColor(ren, 0, 255, 255, 255);
		break;
	case GREEN:
		SDL_SetRenderDrawColor(ren, 0, 180, 0, 255);
		break;
	case BLUE:
		SDL_SetRenderDrawColor(ren, 0, 0, 180, 255);
		break;
	case RED:
		SDL_SetRenderDrawColor(ren, 180, 0, 0, 255);
		break;
	case YELLOW:
		SDL_SetRenderDrawColor(ren, 180, 180, 0, 255);
		break;
	case ORANGE:
		SDL_SetRenderDrawColor(ren, 180, 100, 0, 255);
		break;
	case PINK:
		SDL_SetRenderDrawColor(ren, 180, 0, 140, 255);
		break;
	case MAGENTA:
		SDL_SetRenderDrawColor(ren, 120, 0, 100, 255);
		break;
	default: SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
	}

	SDL_Rect r;
	r.x = x;
	r.y = y;
	r.h = h;
	r.w = w;

	SDL_RenderFillRect(ren, &r);

}