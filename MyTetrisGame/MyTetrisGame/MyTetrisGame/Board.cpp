#include "Board.h"
#include "Event.h"
#include "ResetEvent.h"
#include "Game.h"

/*Public constructor for the board.*/
Board::Board(Pieces *pPieces)
{
	// Get the pointer to the pieces class
	mPieces = pPieces;

	middle = INITIAL_MIDDLE;
	//Init the board blocks with free positions
	InitBoard();
}

/*Init the board blocks with free positions */
void Board::InitBoard()
{
	for (int i = 0; i < BOARD_WIDTH; i++)
		for (int j = 0; j < BOARD_HEIGHT; j++)
			mBoard[i][j] = POS_FREE;
}

/*
======================================
Store a piece in the board by filling the blocks

Parameters:

>> pX:		Horizontal position in blocks
>> pY:		Vertical position in blocks
>> pPiece:	Piece to draw
>> pRotation:	1 of the 4 possible rotations
======================================
*/
void Board::StorePiece(int pX, int pY, int pPiece, int pRotation)
{
	// Store each block of the piece into the board
	for (int i1 = pX, i2 = 0; i1 < pX + PIECE_BLOCKS; i1++, i2++)
	{
		for (int j1 = pY, j2 = 0; j1 < pY + PIECE_BLOCKS; j1++, j2++)
		{
			// Store only the blocks of the piece that are not holes
			if (mPieces->GetBlockType(pPiece, pRotation, j2, i2) != 0)
				mBoard[i1][j1] = POS_FILLED;
		}
	}
}


/*
======================================
Check if the game is over becase one of the players have something on his first line

Returns true or false
======================================
*/
bool Board::IsGameOver()
{
	//If the first line of one of the players has blocks, then game is over
	//If the first line has blocks, then, game over
	Event* e;
	bool p1lost = false;
	bool p2lost = false;
	for (int i = 0; i < BOARD_HEIGHT; i++)
	{
		if (mBoard[0][i] == POS_FILLED) {
			p1lost = true;
		}

		if (mBoard[BOARD_WIDTH - 1][i] == POS_FILLED) {
			p2lost = true;
		}
	}
	if (p1lost && p2lost) { // Draw
		e = new ResetEvent(0);
		Game::eventQueue.push(e);
		return true;
	}
	else if (p1lost) { // Player 2 is the winner
		e = new ResetEvent(2);
		Game::eventQueue.push(e);
		return true;
	}
	else if (p2lost) { // Player 1 is the winner
		e = new ResetEvent(1);
		Game::eventQueue.push(e);
		return true;
	}
	return false;

}



/*Delete a line of the board by moving all above lines from current player towards the middle */
void Board::DeleteLine(int line, int side)
{
	if (side == 1) {

		for (int j = line; j > 0; j--)
			for (int i = 0; i < BOARD_HEIGHT; i++)
				mBoard[j][i] = mBoard[j - 1][i];

		middle++;
		for (int j = BOARD_WIDTH - 1; j >0; j--)
			for (int i = 0; i < BOARD_HEIGHT; i++)
				mBoard[j][i] = mBoard[j - 1][i];
	}
	else {

		for (int j = line; j < BOARD_WIDTH - 1; j++)
			for (int i = 0; i < BOARD_HEIGHT; i++)
				mBoard[j][i] = mBoard[j + 1][i];

		middle--;
		for (int j = 0; j < BOARD_WIDTH - 1; j++)
			for (int i = 0; i < BOARD_HEIGHT; i++)
				mBoard[j][i] = mBoard[j + 1][i];
	}
}


/*Delete all the lines that should be removed and move the middle of the board*/
void Board::DeletePossibleLines()
{

	int left = 1;
	int right = 2;

	for (int j = 0; j < middle; j++)
	{
		int i = 0;
		while (i < BOARD_HEIGHT)
		{
			if (mBoard[j][i] != POS_FILLED)
				break;
			i++;
		}

		if (i == BOARD_HEIGHT)
			DeleteLine(j, left);
	}

	for (int j = BOARD_WIDTH - 1; j > middle - 1; j--)
	{
		int i = 0;
		while (i < BOARD_HEIGHT)
		{
			if (mBoard[j][i] != POS_FILLED)
				break;
			i++;
		}

		if (i == BOARD_HEIGHT)
			DeleteLine(j, right);
	}
}


/*
======================================
Returns 1 (true) if the this block of the board is empty, 0 if it is filled

Parameters:

>> pX:		Horizontal position in blocks
>> pY:		Vertical position in blocks
======================================
*/
bool Board::IsFreeBlock(int pX, int pY)
{
	return (mBoard[pX][pY] == POS_FREE);
}

/*
======================================
Returns the horizontal position (isn pixels) of the block given like parameter

Parameters:

>> pPos:	Horizontal position of the block in the board
======================================
*/
int Board::GetXPosInPixels(int pPos)
{
	return  ((BOARD_POSITION_X - (BLOCK_SIZE * (BOARD_WIDTH / 2))) + (pPos * BLOCK_SIZE));
}


/*
======================================
Returns the vertical position (in pixels) of the block given like parameter

Parameters:

>> pPos:	Horizontal position of the block in the board
======================================
*/
int Board::GetYPosInPixels(int pPos)
{
	return ((BOARD_POSITION_Y - (BLOCK_SIZE * BOARD_HEIGHT)) + (pPos * BLOCK_SIZE));
}


/*
======================================
Check if the piece can be stored at this position without any collision
Returns true if the movement is  possible, false if it not possible

Parameters:
>> side:	Which player tries to move a piece
>> pX:		Horizontal position in blocks
>> pY:		Vertical position in blocks
>> pPiece:	Piece to draw
>> pRotation:	1 of the 4 possible rotations
======================================
*/
bool Board::IsPossibleMovement(int side, int pX, int pY, int pPiece, int pRotation)
{
	// Checks collision with pieces already stored in the board or the board limits
	// This is just to check the 5x5 blocks of a piece with the appropiate area in the board
	for (int i1 = pX, i2 = 0; i1 < pX + PIECE_BLOCKS; i1++, i2++)
	{
		for (int j1 = pY, j2 = 0; j1 < pY + PIECE_BLOCKS; j1++, j2++)
		{
			// Check if the piece is outside the limits of the board
			if (side == 1) {
				if (i1 >= middle || j1 > BOARD_HEIGHT - 1 || j1 < 0 || i1 < 0)
				{
					if (mPieces->GetBlockType(pPiece, pRotation, j2, i2) != 0)
						return false;
				}
			}
			else {
				if (i1 < middle || j1 > BOARD_HEIGHT - 1 || j1 < 0 || i1 > BOARD_WIDTH - 1)
				{
					if (mPieces->GetBlockType(pPiece, pRotation, j2, i2) != 0)
						return false;
				}
			}

			// Check if the piece have collisioned with a block already stored in the map
			if (j1 >= 0)
			{
				if ((mPieces->GetBlockType(pPiece, pRotation, j2, i2) != 0) &&
					(!IsFreeBlock(i1, j1)))
					return false;
			}
		}
	}

	// No collision
	return true;
}


void Board::printP1() {
	int newBoard[BOARD_WIDTH][BOARD_HEIGHT] = {
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 1, 1, 1, 1, 1, 1, 0, 0 },
			{ 0, 0, 0, 0, 0, 1, 0, 1, 0, 0 },
			{ 0, 0, 0, 0, 0, 1, 0, 1, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 1, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 1, 0, 0, 0 },
			{ 0, 0, 1, 1, 1, 1, 1, 1, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 1, 1, 1, 0, 0 },
			{ 0, 0, 0, 1, 1, 1, 0, 0, 0, 0 },
			{ 0, 0, 1, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 1, 1, 0, 0, 0, 0, 0 },
			{ 0, 0, 1, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 1, 1, 1, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 1, 1, 1, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 1, 1, 1, 1, 0, 0, 0 },
			{ 0, 0, 1, 0, 0, 0, 0, 1, 0, 0 },
			{ 0, 0, 1, 0, 0, 0, 0, 1, 0, 0 },
			{ 0, 0, 0, 1, 1, 1, 1, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 1, 1, 1, 1, 1, 1, 0, 0 },
			{ 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 1, 0, 0, 0, 0, 0 },
			{ 0, 0, 1, 1, 1, 1, 1, 1, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }

	};

	for (int i = 0; i < BOARD_WIDTH; i++)
		for (int j = 0; j < BOARD_HEIGHT; j++)
			mBoard[29 - i][9 - j] = newBoard[29 - i][j];
}

void Board::printP2() {
	int newBoard[BOARD_WIDTH][BOARD_HEIGHT] = {
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 1, 1, 1, 1, 1, 1, 0, 0 },
			{ 0, 0, 0, 0, 0, 1, 0, 1, 0, 0 },
			{ 0, 0, 0, 0, 0, 1, 0, 1, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 1, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 1, 1, 0, 0, 1, 0, 0, 0 },
			{ 0, 0, 1, 0, 1, 0, 0, 1, 0, 0 },
			{ 0, 0, 1, 0, 0, 1, 0, 1, 0, 0 },
			{ 0, 0, 1, 0, 0, 0, 1, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 1, 1, 1, 0, 0 },
			{ 0, 0, 0, 1, 1, 1, 0, 0, 0, 0 },
			{ 0, 0, 1, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 1, 1, 0, 0, 0, 0, 0 },
			{ 0, 0, 1, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 1, 1, 1, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 1, 1, 1, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 1, 1, 1, 1, 0, 0, 0 },
			{ 0, 0, 1, 0, 0, 0, 0, 1, 0, 0 },
			{ 0, 0, 1, 0, 0, 0, 0, 1, 0, 0 },
			{ 0, 0, 0, 1, 1, 1, 1, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 1, 1, 1, 1, 1, 1, 0, 0 },
			{ 0, 0, 0, 0, 0, 1, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 1, 0, 0, 0, 0, 0 },
			{ 0, 0, 1, 1, 1, 1, 1, 1, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
	};

	for (int i = 0; i < BOARD_WIDTH; i++)
		for (int j = 0; j < BOARD_HEIGHT; j++)
			mBoard[29 - i][9 - j] = newBoard[29 - i][j];
}

void Board::printDraw() {
	
	int newBoard[BOARD_WIDTH][BOARD_HEIGHT] = {
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 1, 1, 1, 1, 1, 1, 0, 0 },
			{ 0, 0, 1, 0, 0, 0, 0, 1, 0, 0 },
			{ 0, 0, 1, 0, 0, 0, 0, 1, 0, 0 },
			{ 0, 0, 0, 1, 1, 1, 1, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 1, 1, 1, 1, 1, 1, 0, 0 },
			{ 0, 0, 0, 0, 1, 1, 0, 1, 0, 0 },
			{ 0, 0, 0, 1, 0, 1, 0, 1, 0, 0 },
			{ 0, 0, 1, 0, 0, 0, 1, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 1, 1, 1, 1, 1, 0, 0, 0 },
			{ 0, 0, 0, 0, 1, 0, 0, 1, 0, 0 },
			{ 0, 0, 0, 0, 1, 0, 0, 1, 0, 0 },
			{ 0, 0, 1, 1, 1, 1, 1, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 1, 1, 1, 0, 0 },
			{ 0, 0, 0, 1, 1, 1, 0, 0, 0, 0 },
			{ 0, 0, 1, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 1, 1, 0, 0, 0, 0, 0 },
			{ 0, 0, 1, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 1, 1, 1, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 1, 1, 1, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }
	};

	for (int i = 0; i < BOARD_WIDTH; i++)
		for (int j = 0; j < BOARD_HEIGHT; j++)
			mBoard[29 - i][9 - j] = newBoard[29 - i][j];

}