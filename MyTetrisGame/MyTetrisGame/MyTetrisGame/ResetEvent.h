#pragma once
#include "Event.h"
class ResetEvent :
	public Event
{
public:
	ResetEvent(int winner);
	int winner;
};

