#pragma once
#include "Pieces.h"

#define BOARD_LINE_WIDTH 5			// Width of each of the two lines that delimit the board
#define BLOCK_SIZE 25				// Width and Height of each block of a piece
#define BOARD_POSITION_X 125		// Board corner position on OX
#define BOARD_POSITION_Y 150		// Board corner position on OY
#define BOARD_WIDTH 30				// Board width in blocks 
#define BOARD_HEIGHT 10				// Board height in blocks
#define PIECE_BLOCKS 5				// Number of horizontal and vertical blocks of a matrix piece
#define OUTER_POSITION 999			// A position which helps determine if a piece is the next piece 
#define INITIAL_MIDDLE 15			// Initial position of the middle of the board
#define MIDDLE_LINE_WIDTH 3			// Middle line width in pixels

class Board
{
public:

	Board(Pieces *pPieces);
	int middle;
	int GetXPosInPixels(int pPos);
	int GetYPosInPixels(int pPos);
	bool IsFreeBlock(int pX, int pY);
	bool IsPossibleMovement(int side, int pX, int pY, int pPiece, int pRotation);
	void StorePiece(int pX, int pY, int pPiece, int pRotation);
	void DeletePossibleLines();
	bool IsGameOver();
	void printDraw();
	void printP1();
	void printP2();

private:

	enum { POS_FREE, POS_FILLED };			// POS_FREE = free position of the board; POS_FILLED = filled position of the board
	int mBoard[BOARD_WIDTH][BOARD_HEIGHT];	// Board that contains the pieces
	Pieces *mPieces;
	int mScreenHeight;

	void InitBoard();
	void DeleteLine(int line, int side);
};

