#pragma once
#include "SDL.h"

enum color { RED, ORANGE, BLUE, DARK_CYAN, GREEN, CYAN, MAGENTA, YELLOW, WHITE, PINK }; // Colors

class Interface
{
public:
	Interface();
	void DrawRectangle(SDL_Renderer* ren, int x, int y, int h, int w, color c);

};

