#pragma once
class Pieces
{
public:

	int GetBlockType(int pPiece, int pRotation, int pX, int pY);
	int GetXInitialPosition(int side, int pPiece, int pRotation);
	int GetYInitialPosition(int pPiece, int pRotation);
};
