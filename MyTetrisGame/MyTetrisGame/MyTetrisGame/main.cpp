#include "Game.h"
#include "KeyPresses.h"
#include <thread>
#undef main
using namespace std;

/*Method that stars a new thread, used for reading keypresses*/
void runCatcherThread(Game mGame) {
	KeyPresses* k = new KeyPresses();
	k->checkForKeyPresses(mGame);
}

int main(int, char**){

	//Interface
	Interface mInterface;

	// Pieces
	Pieces mPieces;

	// Board
	Board mBoard(&mPieces);

	// Game
	Game mGame(&mBoard, &mPieces, &mInterface);

	// Call method runCatcherThread with argument mGame
	thread eventCatcherThread(runCatcherThread, mGame);

	// run the actual game
	mGame.run();

	return 0;
}