#pragma once
#include "Board.h"
#include "Pieces.h"
#include "Interface.h"
#include <time.h>
#include "SDL.h"
#include <queue>
#include "event.h"
#include "Player.h"
#include <thread>         
#include <chrono>
#include "Interface.h"
#include "DropEvent.h"
#include "MoveEvent.h"
#include "RotateEvent.h"
#include "ResetEvent.h"
#include "SDL_image.h"


#define WINDOW_POSITION_X 100		// Window initial position on OX
#define WINDOW_POSITION_Y 100		// Window initial position on OY
#define WINDOW_WIDTH 1000			// Window width in pixels
#define WINDOW_HEIGHT 500			// Window height in pixels
#define DROP_TIME 700				// Time between two drops

class Game {

public:
	static	std::queue<Event*> eventQueue;
	static  bool hasStarted;
	static  long initialTime;

public:
	Game(Board *pBoard, Pieces *pPieces, Interface *pInterface);
	void run();

private:
	Board *mBoard;
	Pieces *mPieces;
	Interface *mInterface;
	SDL_Texture* background;

	Player* leftPlayer;
	Player* rightPlayer;

	void InitGame();
	void drawPiece(SDL_Renderer* ren, int pX, int pY, int pPiece, int pRotation);
	void drawAllPieces(SDL_Renderer* ren);
	void drawBoard(SDL_Renderer *ren);
	void drawScene(SDL_Renderer *ren);
	void doTheMove();
	void solveEvent(Event* e);
	void solveMoveEvent(int side, int advance, int direction);
	void solveRotateEvent(int side);
	void solveDropEvent(int side);
	void solveResetEvent(int winner);


};

