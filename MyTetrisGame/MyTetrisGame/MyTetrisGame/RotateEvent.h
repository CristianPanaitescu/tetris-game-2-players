#pragma once
#include "Event.h"
class RotateEvent :
	public Event
{
public:
	RotateEvent(int side);
	int side;
};

