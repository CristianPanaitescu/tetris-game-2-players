#pragma once
#include "KeyPresses.h"

KeyPresses::KeyPresses(){}

int KeyPresses::PollKey()
{
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		switch (event.type) {
		case SDL_KEYDOWN:
			return event.key.keysym.sym;
		case SDL_QUIT:
			exit(3);
		}
	}
	return -1;
}

void KeyPresses::checkForKeyPresses(Game mGame) {

	while (true) {
		int key = PollKey();
		if (key == -1)
			continue;
		Event* e;
		switch (key) {
		case SDLK_w:
			e = new MoveEvent(1, 0, 1);
			Game::eventQueue.push(e);
			break;

		case SDLK_s:
			e = new MoveEvent(1, 0, -1);
			Game::eventQueue.push(e);
			break;

		case SDLK_d:
			e = new MoveEvent(1, 1, 0);
			Game::eventQueue.push(e);
			break;

		case SDLK_a:
			e = new RotateEvent(1);
			Game::eventQueue.push(e);
			break;

		case SDLK_TAB:
			e = new DropEvent(1);
			Game::eventQueue.push(e);
			break;

		case SDLK_UP:
			e = new MoveEvent(2, 0, 1);
			Game::eventQueue.push(e);
			break;

		case SDLK_DOWN:
			e = new MoveEvent(2, 0, -1);
			Game::eventQueue.push(e);
			break;

		case SDLK_LEFT:
			e = new MoveEvent(2, -1, 0);
			Game::eventQueue.push(e);
			break;

		case SDLK_RIGHT:
			e = new RotateEvent(2);
			Game::eventQueue.push(e);
			break;

		case SDLK_RCTRL:
			e = new DropEvent(2);
			Game::eventQueue.push(e);
			break;

		case SDLK_RETURN:
			e = new StartEvent();
			Game::eventQueue.push(e);
			break;

		case SDLK_BACKSPACE:
			e = new ResetEvent(-1);
			Game::eventQueue.push(e);
			break;

		case SDLK_ESCAPE:
			e = new ExitEvent();
			Game::eventQueue.push(e);
			break; break;
		}
	}
}
