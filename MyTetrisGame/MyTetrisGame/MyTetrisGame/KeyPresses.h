#pragma once
#include "DropEvent.h"
#include "MoveEvent.h"
#include "RotateEvent.h"
#include "StartEvent.h"
#include "ExitEvent.h"
#include "ResetEvent.h"
#include "Event.h"
#include "Game.h"


class KeyPresses
{
public:

	KeyPresses();
	void checkForKeyPresses(Game mGame);
	int PollKey();
	int GetKey();
};

