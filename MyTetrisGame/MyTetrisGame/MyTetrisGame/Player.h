#pragma once
#include "Pieces.h"

class Player
{
public:

	Player(int side);
	int side; // 1-left | 2-right
	void init(Pieces* mPieces);
	void CreateNewPiece();
	Pieces* mPieces;

public:
	int mPosX, mPosY;
	int mPiece, mRotation;

	int mNextPosX, mNextPosY;		// Position of the next piece
	int mNextPiece, mNextRotation;	// Kind and rotation of the next piece

private:
	int GetRand(int pA, int pB);

};