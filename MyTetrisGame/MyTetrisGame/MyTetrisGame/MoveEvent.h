#pragma once
#include "Event.h"
class MoveEvent :
	public Event
{
public:
	MoveEvent(int side, int advance, int direction);
	int side;
	int advance;
	int direction;
};

