#include "Player.h"
#include "Board.h"
#include <chrono>

Player::Player(int side) {
	this->side = side;
}

int Player::GetRand(int pA, int pB)
{
	return (rand() + side) % (pB - pA + 1) + pA;
}

void Player::init(Pieces* mPieces) {

	this->mPieces = mPieces;
	// Init random numbers
	srand((unsigned int)time(NULL));

	// First piece
	mPiece = GetRand(0, 6);
	mRotation = GetRand(0, 3);
	if (side == 1)
		mPosX = mPieces->GetXInitialPosition(side, mPiece, mRotation);
	else {

		mPosX = BOARD_WIDTH - (PIECE_BLOCKS + mPieces->GetXInitialPosition(side, mPiece, mRotation));
		if (mPiece == 0)
			mPosX--;
		mRotation = (mRotation + 2) % 4;
	}
	mPosY = (BOARD_HEIGHT / 2) + mPieces->GetYInitialPosition(mPiece, mRotation);

	//  Next piece
	mNextPiece = GetRand(0, 6);
	mNextRotation = GetRand(0, 3);
	if (side == 2)
		mNextRotation = (mNextRotation + 2) % 4;
	mNextPosX = mPosX;
	mNextPosY = OUTER_POSITION;

}

void Player::CreateNewPiece()
{
	// The new piece
	mPiece = mNextPiece;
	mRotation = mNextRotation;
	if (side == 2)
		mRotation = (mRotation + 2) % 4;
	if (side == 1)
		mPosX = mPieces->GetXInitialPosition(side, mPiece, mRotation);
	else {

		mPosX = BOARD_WIDTH - (PIECE_BLOCKS + mPieces->GetXInitialPosition(side, mPiece, mRotation));
		if (mPiece == 0)
			mPosX--;
		mRotation = (mRotation + 2) % 4;
	}
	mPosY = (BOARD_HEIGHT / 2) + mPieces->GetYInitialPosition(mPiece, mRotation);

	// Random next piece
	mNextPiece = GetRand(0, 6);
	mNextRotation = GetRand(0, 3);

}
